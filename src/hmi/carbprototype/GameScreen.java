package hmi.carbprototype;

import hmi.carbprototype.views.DrawView;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.plattysoft.leonids.ParticleSystem;

/**
 * This shows the game screen after a level was selected
 * @author I.H. Pouw
 */
public class GameScreen extends Activity {
	protected Toast myToast;
	protected String level;
	protected boolean difHard;

	@SuppressLint("ShowToast")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		myToast = Toast.makeText(getBaseContext(), "", Toast.LENGTH_SHORT);
		Intent i = getIntent();
		level = i.getStringExtra("level");
		difHard = i.getBooleanExtra("difficultyHard", false);
		mediaPlayer = MediaPlayer.create(getApplicationContext(),
				R.raw.score);

		setContentView(R.layout.main);
		TextView goal = (TextView) findViewById(R.id.Aim);
		TextView score = (TextView) findViewById(R.id.Score);
		DrawView draw = (DrawView) findViewById(R.id.Draw);
		ProgressBar pb = (ProgressBar) findViewById(R.id.progressbar);
		draw.setGoalView(goal);
		draw.setScoreView(score);
		draw.setParent(this);
		draw.setProgressBar(pb);
		draw.init(this.level, this.difHard);
		showHelp(null);
	}
	
	public void displayMessage(final String msg, final int res) {
		runOnUiThread(new Runnable() {
			public void run() {
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_layout,
				                               (ViewGroup) findViewById(R.id.toast_layout_root));

				TextView text = (TextView) layout.findViewById(R.id.text);
				if(res > 0) {
					text.setText(res);
				} else {
					text.setText(msg);	
				}
				
				myToast.setView(layout);
				myToast.show();
			}
		});
	}

	protected Dialog mDialog;

	@SuppressLint("InflateParams")
	public void showHelp(View view) {
		AlertDialog.Builder builder;
		LayoutInflater inflater = (LayoutInflater) this.getLayoutInflater();
		View layout = inflater.inflate(getHelp(), null);
		Button button = (Button) layout.findViewById(R.id.button1);
		button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});
		TextView text = (TextView) layout.findViewById(R.id.text1);
		
		if(!difHard) {
			text.setText("Kies drie etenswaren uit de tabel rechts zodat het totaal aantal gram koolhydraten gelijk is aan het doel links");
		} else {
			text.setText("Veeg drie etenswaren op een rij uit de tabel rechts zodat het totaal aantal gram koolhydraten gelijk is aan het doel links");
		}
		
		builder = new AlertDialog.Builder(this);
		builder.setView(layout);

		
		
		mDialog = builder.create();
		mDialog.show();
		//mDialog.getWindow().setLayout(1200, 700);
	}
	
	protected int getHelp() {
		if(level.equals("1")) {
			return R.layout.help;
		} else if (level.equals("2")) {
			return R.layout.help2;
		} else if (level.equals("3")) {
			return R.layout.help3;
		} else if (level.equals("4")) {
			return R.layout.help4;
		} else {
			return R.layout.help5;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	protected void onStop() {
		super.onStop();
		mediaPlayer.release();
		mediaPlayer = null;
		
		this.finish();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mediaPlayer != null) {
			mediaPlayer.release();
			mediaPlayer = null;
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onRestart()
	 */
	@Override
	protected void onRestart() {
		super.onRestart();
		mediaPlayer = MediaPlayer.create(getApplicationContext(),
				R.raw.score);
	}
	
	public void exitButton(View view) {
		this.finish();
	}

	public void endLevel(long time) {
		new ParticleSystem(this, 100, R.drawable.star_pink, 800)
		.setSpeedRange(0.1f, 0.25f)
		.oneShot(findViewById(R.id.emiter_top_progressbar), 100);
				
		long seconds = TimeUnit.SECONDS.convert(time, TimeUnit.NANOSECONDS);
		long minutes = TimeUnit.MINUTES.convert(time,  TimeUnit.NANOSECONDS);
		if(minutes > 0) {
			seconds -= 60 * minutes;
		}
		String secondsStr = "" + seconds;
		if(seconds < 10) {
			secondsStr = "0" + secondsStr;
		}
		final String timeStr = minutes + ":" + secondsStr;
		minutes = Math.round(minutes / 2);
    	long stars = 3 - minutes;
    	if(stars < 0) {
    		stars = 0;
    	}
    	if(stars > 0 && difHard) {
    		stars = stars + 3;
    	}
    	final long stars2 = stars;
		
		SharedPreferences sharedPref = getSharedPreferences(
		        getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		int currentLevel = sharedPref.getInt("current", 0);
		int level = Integer.parseInt(this.level);
		if(level > currentLevel) {
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putInt("current", level);
			editor.putBoolean("newLevelAnim", true);
			editor.commit();
		}
		long currentStars = sharedPref.getLong("stars" + level, 0);
		if(stars > currentStars) {
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putLong("stars" + level, stars);
			editor.commit();
		}
		
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
		    @Override
		    public void run() {
		    	showEnding(timeStr, stars2);
		    }
		}, 1200);

		handler.postDelayed(new Runnable() {
		    @Override
		    public void run() {
		    	mediaPlayer.setVolume(0.8f, 0.8f);
		    	playSample(3);
		    }
		}, 200);
	}
	
	@SuppressLint("InflateParams") public void showEnding(String time, long stars) {
		final GameScreen parent = this;
		LayoutInflater inflater = (LayoutInflater) this.getLayoutInflater();
		View layout = inflater.inflate(R.layout.finish, (ViewGroup) findViewById(R.id.root_view));
		Button button = (Button) layout.findViewById(R.id.button1);
		button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mediaPlayer.reset();
				parent.finish();
			}
		});
		
		TextView timeView = (TextView) layout.findViewById(R.id.time);
		timeView.setText(time);
		ImageView bar = (ImageView) layout.findViewById(R.id.starBar);
		if(stars == 1) {
			bar.setImageResource(R.drawable.bar1);
		}
		if(stars == 2) {
			bar.setImageResource(R.drawable.bar2);
		}
		if(stars == 3) {
			bar.setImageResource(R.drawable.bar3);
		}
		if(stars == 4) {
			bar.setImageResource(R.drawable.bar4);
		}
		if(stars == 5) {
			bar.setImageResource(R.drawable.bar5);
		}
		if(stars == 6) {
			bar.setImageResource(R.drawable.bar6);
		}
		ImageView food = (ImageView) layout.findViewById(R.id.food);
		if("1".equals(level)) {
			food.setImageResource(R.drawable.price1);
		}
		if("2".equals(level)) {
			food.setImageResource(R.drawable.price2);
		}
		if("3".equals(level)) {
			food.setImageResource(R.drawable.price3);
		}
		if("4".equals(level)) {
			food.setImageResource(R.drawable.price4);
		}
		if("5".equals(level)) {
			food.setImageResource(R.drawable.price5);
		}
		
		new ParticleSystem(this, 100, R.drawable.star_pink, 800)
		.setSpeedRange(0.1f, 0.25f)
		.oneShot(findViewById(R.id.emiter_top1), 100);
		new ParticleSystem(this, 100, R.drawable.star_white, 800)
		.setSpeedRange(0.1f, 0.25f)
		.oneShot(findViewById(R.id.emiter_top1), 100);
		new ParticleSystem(this, 100, R.drawable.star_pink, 800)
		.setSpeedRange(0.1f, 0.25f)
		.oneShot(findViewById(R.id.emiter_top2), 100);
		new ParticleSystem(this, 100, R.drawable.star_white, 800)
		.setSpeedRange(0.1f, 0.25f)
		.oneShot(findViewById(R.id.emiter_top2), 100);
		new ParticleSystem(this, 100, R.drawable.star_pink, 800)
		.setSpeedRange(0.1f, 0.25f)
		.oneShot(findViewById(R.id.emiter_top3), 100);
		new ParticleSystem(this, 100, R.drawable.star_white, 800)
		.setSpeedRange(0.1f, 0.25f)
		.oneShot(findViewById(R.id.emiter_top3), 100);
	}
	
	private MediaPlayer mediaPlayer;
	private int[] soundMatrix = {R.raw.score, R.raw.wrong, R.raw.won};
	public void playSample(int index) {
		int resid = soundMatrix[index - 1];
		AssetFileDescriptor afd = getResources().openRawResourceFd(resid);

		try {
			mediaPlayer.reset();
			mediaPlayer.setDataSource(afd.getFileDescriptor(),
					afd.getStartOffset(), afd.getDeclaredLength());
			mediaPlayer.prepare();
			mediaPlayer.start();
			afd.close();
		} catch (IllegalArgumentException e) {
			Log.e("Pref",
					"Unable to play audio queue due to exception: "
							+ e.getMessage(), e);
		} catch (IllegalStateException e) {
			Log.e("Pref",
					"Unable to play audio queue due to exception: "
							+ e.getMessage(), e);
		} catch (IOException e) {
			Log.e("Pref",
					"Unable to play audio queue due to exception: "
							+ e.getMessage(), e);
		}
	}
}
