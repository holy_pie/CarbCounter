package hmi.carbprototype.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class SquareView extends RelativeLayout
{

    public SquareView(final Context context)
    {
        super(context);
    }

    public SquareView(final Context context, final AttributeSet attrs)
    {
        super(context, attrs);
    }

    public SquareView(final Context context, final AttributeSet attrs, final int defStyle)
    {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec)
    {
    	super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        final int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        final int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        final int height = getDefaultSize(heightSize, heightMeasureSpec);
        final int width = getDefaultSize(widthSize, widthMeasureSpec);
        int res = 0;
        if(width < height) {
        	res = width;
        } else {
        	res = height;
        }
        setMeasuredDimension(res, res);
        
    }

    @Override
    protected void onSizeChanged(final int w, final int h, final int oldw, final int oldh)
    {
        super.onSizeChanged(w, w, oldw, oldh);
    }
}