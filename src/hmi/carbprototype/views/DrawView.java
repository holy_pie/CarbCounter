package hmi.carbprototype.views;

import hmi.carbprototype.GameScreen;
import hmi.carbprototype.LevelConfig;
import hmi.carbprototype.R;

import java.util.ArrayList;
import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * This draws the 5 by 5 square with food items in it
 * @author I.H. Pouw
 */
public class DrawView extends View implements Runnable {
	private Paint paint = new Paint();
	private final int rows = 5;
	private final int cols = 5;
	private final int speed = 6;
	private Bitmap[] bmps;
	private Rect rect = new Rect(0, 0, 0, 0);
	private ArrayList<Tile> selectedArray = new ArrayList<Tile>();
	private int goal = 0;
	private int score = 0;
	private TextView goalView;
	private TextView scoreView;
	private GameScreen parent;
	private LevelConfig.Config config;
	private Tile[][] tileArray = new Tile[rows][cols];
	private boolean initialized = false;
	private boolean finished = false;
	private boolean moving = false;
	private int lastScore = 0;
	private ProgressBar pb;
	long startTime;

	// A tile in the grid
	private class Tile {
		public int row;
		public int col;
		public int carbs;
		public int delta = 0;
		public boolean selected = false;
		public Bitmap bmp;

		public void toggle() {
			if (selected) {
				selectedArray.remove(this);
			} else {
				selectedArray.add(this);
			}
			selected = !selected;
		}

		public Tile(int row, int col, Bitmap bmp, int carbs) {
			this.row = row;
			this.col = col;
			this.bmp = bmp;
			this.carbs = carbs;
		}
		
		public boolean equals(Object o) {
			if(o instanceof Tile) {
				Tile t = (Tile) o;
				return t.col == this.col && t.row == this.row;
			}
			return false;
		}
	}
	
	private void addScore(int times) {
		if(times > 0) {
			parent.playSample(1);
		} else {
			parent.playSample(2);
		}
		int score = lastScore + 10;
		lastScore = score;
		score *= times;
		this.score += score;
		if (scoreView != null) {
			scoreView.setText("" + this.score);
		}
		if (pb != null) {
			pb.setProgress(this.score);
		}
		if(this.score >= config.maxScore) {
			finished = true;
			long endTime = System.nanoTime();
			parent.endLevel(endTime - startTime);
		}
	}

	private Tile getNewTile(int row, int col) {
		Random rand = new Random();
		int n = rand.nextInt(config.foodArray.size()); // Gives n such that 0 <= n < size
		return new Tile(row, col, bmps[n], config.foodArray.valueAt(n));
	}

	private void setNewGoal() {
		Random rand = new Random();
		ArrayList<Integer> randomList = new ArrayList<Integer>();
		
		if(config.swipeThree) {
			// get one random unique number
			int n = rand.nextInt(rows * cols);
			randomList.add(n);
			boolean h = rand.nextBoolean();
			boolean d = rand.nextBoolean();
			int row = n / cols;
			int col = n % cols;

			while(randomList.size() < 3) {
				if(h) { // horizontal
					if(d) { // moving right
						col++;
					} else { // moving left
						col--;
					}
				} else { // vertical
					if(d) { // moving down
						row++;
					} else { // moving up
						row--;
					}					
				}
				if(col < 0 || col >= cols || row < 0 || row >= rows) { 
					d = !d; // reached the edge
				} else {
					int index = row * cols + col;
					if(!randomList.contains(index)) {
						randomList.add(index);
					}
				}
			}
		} else {
			// get three random unique numbers
			while(randomList.size() < 3) {
				int n = rand.nextInt(rows * cols);
				if(!randomList.contains(n)) {
					randomList.add(n);
				}
			}
			
		}
		
		int score = 0;
		for (Integer n : randomList) {
			int row = n.intValue() / cols;
			int col = n.intValue() % cols;
			score += tileArray[row][col].carbs;
		}
		goal = score;
		if (goalView != null) {
			goalView.setText("" + goal);
		}
	}

	public void setGoalView(TextView goalView) {
		this.goalView = goalView;
		if (goalView != null) {
			goalView.setText("" + goal);
		}
	}
	public void setScoreView(TextView scoreView) {
		this.scoreView = scoreView;
		if (scoreView != null) {
			scoreView.setText("" + score);
		}
	}
	public void setParent(GameScreen parent) {
		this.parent = parent;
	}

	@Override
	public void run() {
		// Request a redraw of this view
		// onDraw(Canvas) will be called
		invalidate();
	}

	public void init(String level, boolean difHard) {
		config = LevelConfig.getConfig(Integer.valueOf(level));
		if(difHard) {
			config.swipeThree = true;
		} else {
			config.swipeThree = false;
		}
		paint.setStrokeWidth(5);
		bmps = new Bitmap[config.foodArray.size()];
		for(int i = 0; i < config.foodArray.size(); i++) {
			int resource = config.foodArray.keyAt(i);
			bmps[i] = BitmapFactory.decodeResource(getResources(), resource);
		}
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				tileArray[i][j] = getNewTile(i, j);
			}
		}
		if(pb != null) {
			pb.setMax(config.maxScore);
		}
		moving = true; // this ensures a set new goal()
		initialized = true;
		startTime = System.nanoTime();
	}

	@Override
	public void onDraw(Canvas canvas) {
		if(!initialized && !isInEditMode()) {
			return;
		}
		int width = getMeasuredWidth();
		int height = getMeasuredHeight();
		int unit = Math.min(width, height) / 5;
		boolean check = false;

		if(isInEditMode()){
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					paint.setColor(Color.WHITE);
					canvas.drawCircle(unit / 2 + j * unit, unit / 2 + i * unit
							, unit / 2, paint);
				}
			}
			return;
		}
		
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Tile tile = tileArray[i][j];
				if (tile.selected) {
					paint.setColor(Color.WHITE);
				} else {
					paint.setColor(Color.CYAN);
				}
				canvas.drawCircle(unit / 2 + j * unit, unit / 2 + i * unit
						- tile.delta, unit / 2, paint);
				rect.left = j * unit;
				rect.top = i * unit - tile.delta;
				rect.right = rect.left + unit;
				rect.bottom = rect.top + unit;
				canvas.drawBitmap(tile.bmp, null, rect, paint);
				if (tile.delta > 0) {
					tile.delta -= unit / speed;
					check = true;
					if (tile.delta < 0) {
						tile.delta = 0;
					}
				}
			}
		}
		
		if(!check) {
			if(checkFive(unit)) {
				moving = true;
			} else if(moving) {
				moving = false;
				lastScore = 0;
				setNewGoal();
			}
		}

		if (moving) {
			postDelayed(this, 16);
		}
	}

	private boolean checkFive(int unit) {
		if(finished) {
			return false;
		}
		for (int i = 0; i < rows; i++) {
			Bitmap b = tileArray[i][0].bmp;
			boolean same = true;
			for (int j = 1; j < cols; j++) {
				if(b != tileArray[i][j].bmp) {
					same = false;
					break;
				}
			}
			if(same) {
				for (int j = 0; j < cols; j++) {
					removeTile(tileArray[i][j], unit);
				}
				addScore(5);
				return true;
			}
		}
		
		for (int i = 0; i < cols; i++) {
			Bitmap b = tileArray[0][i].bmp;
			boolean same = true;
			for (int j = 1; j < rows; j++) {
				if(b != tileArray[j][i].bmp) {
					same = false;
					break;
				}
			}
			if(same) {
				for (int j = 0; j < rows; j++) {
					removeTile(tileArray[j][i], unit);
				}
				addScore(5);
				return true;
			}
		}
		return false;
	}

	private boolean wrongAgain = false;
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(!initialized || moving) {
			return false;
		}
		float eventX = event.getX();
		float eventY = event.getY();
		int width = getMeasuredWidth();
		int height = getMeasuredHeight();
		int unit = Math.min(width, height) / 5;
		int x = (int) eventX / unit;
		int y = (int) eventY / unit;

		if(config.swipeThree) {
			boolean res = detectSwipe(x, y, event);
			if(!res) return false;
		} else {
			boolean res = detectTouch(x, y, event);
			if(!res) return false;
		}

		if (selectedArray.size() == 3 && event.getAction() ==  MotionEvent.ACTION_UP) {
			// calculate total carbs
			int score = 0;
			for(Tile tile : selectedArray) {
				score += tile.carbs;
			}
			
			if (score == goal) {
				parent.displayMessage("Goed!", R.string.good);
				for (Tile tile : selectedArray) {
					removeTile(tile, unit);
				}
				selectedArray.clear();
				addScore(3);
				moving = true;
				wrongAgain = false;
			} else {
				int res = R.string.wrong1;
				if(wrongAgain) {
					res = R.string.wrong2;
				}
				parent.displayMessage("Fout!", res);
				for (Tile tile : selectedArray) {
					tile.selected = false;
				}
				addScore(-3);
				selectedArray.clear();
				lastScore = 0;
				wrongAgain = true;
			}

		}

		// Schedules a repaint.
		invalidate();
		return true;
	}

	private boolean detectSwipe(int x, int y, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if (x >= cols || y >= rows || x < 0 || y < 0) {
				return false;
			}
			tileArray[y][x].toggle();
			break;
		case MotionEvent.ACTION_MOVE:
			if (x >= cols || y >= rows || x < 0 || y < 0) {
				return false;
			}
			if(selectedArray.size() == 0) {
				tileArray[y][x].toggle();
			}
			if(selectedArray.size() < 2 && !selectedArray.contains(tileArray[y][x])) {
				if(
						(selectedArray.get(0).row == y && selectedArray.get(0).col != x) ||
						(selectedArray.get(0).row != y && selectedArray.get(0).col == x)) {
					tileArray[y][x].toggle();
				} else {
					return false;
				}
			}
			if(selectedArray.size() < 3 && !selectedArray.contains(tileArray[y][x])) {
				if(
						(selectedArray.get(0).row == y && selectedArray.get(0).col != x &&
						selectedArray.get(1).row == y && selectedArray.get(1).col != x) ||
						(selectedArray.get(0).row != y && selectedArray.get(0).col == x &&
						selectedArray.get(1).row != y && selectedArray.get(1).col == x)
				) {
					tileArray[y][x].toggle();
				} else {
					return false;
				}
			}
			break;
		case MotionEvent.ACTION_UP:
			if(selectedArray.size() == 3) {
				break;
			} else {
				for (Tile tile : selectedArray) {
					tile.selected = false;
				}
				selectedArray.clear();
			}
			break;
		default:
			return false;
		}
		invalidate();
		return true;
	}

	private boolean detectTouch(int x, int y, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if (x >= cols || y >= rows) {
				return false;
			}
			tileArray[y][x].toggle();
			break;
		case MotionEvent.ACTION_UP:
			break;
		default:
			return false;
		}
		return true;
	}

	private void removeTile(Tile tile, int unit) {
		int delta = unit;
		for (int i = tile.row; i > 0; i--) {
			tileArray[i][tile.col] = tileArray[i - 1][tile.col];
			tileArray[i][tile.col].row++;
			delta = tileArray[i][tile.col].delta;
			tileArray[i][tile.col].delta = delta + unit;
			tileArray[i][tile.col].selected = false;
		}
		tileArray[0][tile.col] = getNewTile(0, tile.col);
		tileArray[0][tile.col].delta = delta + unit;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int desiredWidth = 100;
		int desiredHeight = 100;

		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);

		int width;
		int height;

		// Measure Width
		if (widthMode == MeasureSpec.EXACTLY) {
			// Must be this size
			width = widthSize;
		} else if (widthMode == MeasureSpec.AT_MOST) {
			// Can't be bigger than...
			width = Math.min(desiredWidth, widthSize);
		} else {
			// Be whatever you want
			width = desiredWidth;
		}

		desiredHeight = width;

		// Measure Height
		if (heightMode == MeasureSpec.EXACTLY) {
			// Must be this size
			height = heightSize;
		} else if (heightMode == MeasureSpec.AT_MOST) {
			// Can't be bigger than...
			height = Math.min(desiredHeight, heightSize);
		} else {
			// Be whatever you want
			height = desiredHeight;
		}

		// MUST CALL THIS
		setMeasuredDimension(width, height);
	}
	
	// constructors
	public DrawView(Context context) {
		super(context);
	}
	
	public DrawView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public DrawView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public void setProgressBar(ProgressBar pb) {
		this.pb = pb;
		if(config != null) {
			pb.setMax(config.maxScore);
		}
	}
}