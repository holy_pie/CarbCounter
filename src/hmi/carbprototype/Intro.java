package hmi.carbprototype;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

/**
 * This shows the level select screen
 * 
 * @author I.H. Pouw
 */
public class Intro extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.intro);
	}

	public void jaButton(View view) {
		Intent i = new Intent(getApplicationContext(), LevelSelect.class);
		startActivity(i);
	}
	
	public void exitButton(View view) {
		this.finish();
	}
	
	@Override
	protected void onStop() {
		super.onStop();

		this.finish();
	}
}
