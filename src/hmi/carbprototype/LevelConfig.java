package hmi.carbprototype;

import android.util.SparseIntArray;

/**
 * This file holds the configuration of each level
 * @author I.H. Pouw
 */
public class LevelConfig {

	public static class Config {
		public SparseIntArray foodArray;
		public boolean powerUp = false;
		public boolean swipeThree = false;
		public int maxScore = 480;
		
		public Config(int[] food, int[] carbs) {
			foodArray = new SparseIntArray();
			for(int i = 0; i < food.length; i++) {
				foodArray.put(food[i], carbs[i]);
			}
		}
	}

	public static Config getConfig(int level) {
		switch (level) {
		case 1:
		{
			int[] food = {R.drawable.egg, R.drawable.tomato, R.drawable.banana};
			int[] carbs = {0, 2, 30};
			return new Config(food, carbs);
		}
		case 2:
		{
			int[] food = {R.drawable.cheese, R.drawable.watermelon, R.drawable.apple};
			int[] carbs = {0, 10, 15};
			return new Config(food, carbs);
		}
		case 3:
		{
			int[] food = {R.drawable.broccoli, R.drawable.carrots, R.drawable.green_pepper};
			int[] carbs = {1, 2, 5};
			return new Config(food, carbs);
		}
		case 4:
		{
			int[] food = {R.drawable.tea, R.drawable.orange, R.drawable.milkshake};
			int[] carbs = {0, 15, 25};
			return new Config(food, carbs);
		}
		case 5: 
		{
			int[] food = {R.drawable.grapes, R.drawable.hamburger, R.drawable.popcorn};
			int[] carbs = {20, 30, 50};
			return new Config(food, carbs);
		}
		default:
			return null;
		}
	}
}
