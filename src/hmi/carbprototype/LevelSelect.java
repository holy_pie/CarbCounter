package hmi.carbprototype;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.plattysoft.leonids.ParticleSystem;
import com.plattysoft.leonids.modifiers.ScaleModifier;

/**
 * This shows the level select screen
 * 
 * @author I.H. Pouw
 */
public class LevelSelect extends Activity {
	private SharedPreferences sharedPref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.level);
		this.sharedPref = getSharedPreferences(
				getString(R.string.preference_file_key), Context.MODE_PRIVATE);
	}

	@Override
	protected void onResume() {
		super.onResume();
		int currentLevel = sharedPref.getInt("current", 0);
		int[] buttons = { R.id.button1, R.id.button2, R.id.button3, R.id.button4, R.id.button5 };
		int[] bars = { R.id.starBar1, R.id.starBar2, R.id.starBar3, R.id.starBar4, R.id.starBar5 };
		ImageButton currentLevelButton = null;
		for (int i = 0; i < buttons.length; i++) {
			ImageButton button = (ImageButton) findViewById(buttons[i]);

			if (i > currentLevel) {
				button.setClickable(false);
				setImage(button, i, "r");
			} else if (i == currentLevel) {
				button.setClickable(true);
				setImage(button, i, "y");
				currentLevelButton = button;
			} else {
				button.setClickable(true);
				setImage(button, i, "g");
				long stars = sharedPref.getLong("stars" + (i + 1), 0);
				ImageView bar = (ImageView) findViewById(bars[i]);
				if(stars == 0) {
					bar.setImageResource(R.drawable.bar00);
				}
				if(stars == 1) {
					bar.setImageResource(R.drawable.bar01);
				}
				if(stars == 2) {
					bar.setImageResource(R.drawable.bar02);
				}
				if(stars == 3) {
					bar.setImageResource(R.drawable.bar03);
				}
				if(stars == 4) {
					bar.setImageResource(R.drawable.bar04);
				}
				if(stars == 5) {
					bar.setImageResource(R.drawable.bar05);
				}
				if(stars == 6) {
					bar.setImageResource(R.drawable.bar06);
				}
			}
		}

		if (sharedPref.getBoolean("newLevelAnim", true) && currentLevelButton != null) {
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putBoolean("newLevelAnim", false);
			editor.commit();
			final Handler handler = new Handler();
			final ImageButton current = currentLevelButton;
			handler.postDelayed(new Runnable() {
			    @Override
			    public void run() {
			    	newLevelAnim(current);
			    }
			}, 1000);
		}
	};
	
	private void newLevelAnim(ImageButton button) {
		new ParticleSystem(this, 10, R.drawable.star, 3000)
		.setSpeedByComponentsRange(-0.1f, 0.1f, -0.1f, 0.02f)
		.setAcceleration(0.000003f, 90)
		.setInitialRotationRange(0, 360).setRotationSpeed(120)
		.setFadeOut(2000)
		.addModifier(new ScaleModifier(0f, 1.5f, 0, 1500))
		.oneShot(button, 10);
	}

	private void setImage(ImageButton button, int i, String res) {
		String res2 = "level" + Integer.toString(i + 1) + res;
		int resId = this.getResources().getIdentifier(res2, "drawable",
				this.getPackageName());
		button.setBackgroundResource(resId);
	}

	public void resetLevel(View view) {
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putInt("current", 0);
		editor.commit();
		onResume();
	}

	public void goLevel(View view) {
		if (!view.isClickable()) {
			return;
		}
		String lvl = "1";
		if (view.getId() == R.id.button1) {
			lvl = "1";
		} else if (view.getId() == R.id.button2) {
			lvl = "2";
		} else if (view.getId() == R.id.button3) {
			lvl = "3";
		} else if (view.getId() == R.id.button4) {
			lvl = "4";
		} else if (view.getId() == R.id.button5) {
			lvl = "5";
		}

		showDifficultySelect(lvl);
	}

	private Dialog mDialog;

	@SuppressLint("InflateParams")
	// not used anymore
	public void showDifficultySelect(final CharSequence level) {
		AlertDialog.Builder builder;
		LayoutInflater inflater = (LayoutInflater) this.getLayoutInflater();
		View layout = inflater.inflate(R.layout.dif_select, null);
		Button button = (Button) layout.findViewById(R.id.button1);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						GameScreen.class);
				i.putExtra("level", level);
				i.putExtra("difficultyHard", false);
				startActivity(i);
				mDialog.dismiss();
			}
		});
		button = (Button) layout.findViewById(R.id.button2);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				runOnUiThread(new Runnable() {
					public void run() {
						Intent i = new Intent(getApplicationContext(),
								GameScreen.class);
						i.putExtra("level", level);
						i.putExtra("difficultyHard", true);
						startActivity(i);
						mDialog.dismiss();
					}
				});
			}
		});

		builder = new AlertDialog.Builder(this);
		builder.setView(layout);

		mDialog = builder.create();
		mDialog.show();
	}

	public void exitButton(View view) {
		this.finish();
	}
}
